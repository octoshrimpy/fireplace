$(document).ready(function()
{ 

  function Spark() {
    this.x;
    this.y;
    this.combo;
    this.color = '#16a085';
    this.size = 30;
    this.gutter = 3;
    this.borderRadius = 100;
    this.fadeOut = 5;
    this.fadeIn = 1;
  }
  
  let currentLocations = []

  Spark.prototype = {
    init: function() {
      
        this.x = getRandom($('.main').width(), this.size);
        this.y = getRandom($('.main').height(), this.size);
        this.combo = "" + this.x + this.y;
        if(currentLocations.indexOf(this.combo) > -1) {
          return;
        }
      
      currentLocations.push(this.combo);
      
      var spark = $('<div>')
        .addClass('spark')
        .css('position', 'absolute')
        .css('left', this.x)
        .css('top', this.y)
        .css('border-radius', this.borderRadius/2 +'%')
        .css('background', this.color)
        .css('height', this.size-this.gutter +'px')
        .css('width', this.size-this.gutter +'px')
        .css('display', 'none')
        .css('box-shadow', '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)');

      $('body').append(spark);
      spark.fadeIn(this.fadeIn * 1000);
      spark.fadeOut(this.fadeOut * 1000, function(){
        currentLocations.splice(currentLocations.indexOf(this.combo), 1);
        // console.log(currentLocations);
        spark.remove();
      }.bind(this));
    }
  }
  
  function getRandom(max, size){
    let random = Math.random() * (max - size) - size;
    
    return Math.ceil(random / size) * size;
  }
  
  function getUnix(){
    return Math.round((new Date()).getTime() / 1000);
  }
  

  setInterval( function(){
    new Spark().init();

  }, 30);
  
});





